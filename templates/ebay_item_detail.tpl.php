<div class='ebay-item ebay-item-detail'>
    <h2 class='title'><?php echo l($element->title, $element->url, array('attributes'=> array('target' => '_blank'))); ?></h2>
    <img src='<?php echo $element->galleryurl; ?>'/> 
    <ul>
    	<li><span>Price</span> $<?php echo $element->price; ?></li>
        <li><span>Buy Now</span> <?php echo ($element->buynow ? 'Yes' : 'No'); ?></li>
        <li><span>Country</span> <?php echo $element->country; ?></li>
        <li><span>Auction ends</span> <?php echo date('c', $element->endtime); ?></li>
    </ul>
</div>