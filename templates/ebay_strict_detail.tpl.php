<div class='ebay-item ebay-strict-detail'>
    <ul>
    	<li><span>Price</span> $<?php echo $element->price; ?></li>
        <li><span>Buy Now</span> <?php echo ($element->buynow ? 'Yes' : 'No'); ?></li>
        <li><span>Country</span> <?php echo $element->country; ?></li>
        <li><span>Auction ends</span> <?php echo date('c', $element->endtime); ?></li>
    </ul>
</div>
